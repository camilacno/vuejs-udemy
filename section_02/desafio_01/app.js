new Vue({
  el: '#desafio',
  data: {
    nome: 'Camila Nepomuceno',
    idade: 39,
    imagem: 'https://vuejs.org/images/logo.png',
  },
  methods: {
    idade3: function() {      
      return this.idade * 3;
    },
    random: function() {
      return Math.random();
    }
  }
})